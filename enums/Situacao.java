package com.deeborges.enums;

public enum Situacao {
    ABERTA(1, "Aberta"),
    FECHADA(2, "Fechada"),
    CANCELADA(3, "Cancelada"),
    FINALIZADA(4, "Finalizada");

    private final int ID;
    private final String DESCRICAO;

    private Situacao(int id, String descricao) {
        this.ID = id;
        this.DESCRICAO = descricao;
    }

    public int getId() {
        return ID;
    }

    @Override
    public String toString() {
        return this.DESCRICAO;
    }
}