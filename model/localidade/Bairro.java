package com.deeborges.model.localidade;

public class Bairro {
    private int codigo;
    private String nome;
    private int codigo_cidade;

    public Bairro() {
        this.codigo = 0;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo_cidade() {
        return codigo_cidade;
    }

    public void setCodigo_cidade(int codigo_cidade) {
        this.codigo_cidade = codigo_cidade;
    } 
}
